# List Produtos

- [Tecnologias](#tecnologias)

- [Instalação](#instalação)

- [Descrição](#descrição)

- [Routes](#routes)


## Tecnologias

### Linux
[PHP => 7.4](https://www.digitalocean.com/community/tutorials/how-to-install-php-7-4-and-set-up-a-local-development-environment-on-ubuntu-20-04)

[MySQL 5.7 || MySQL 8](https://www.digitalocean.com/community/tutorials/how-to-install-mysql-on-ubuntu-20-04-pt)  

### Windows
 [PHP => 7.4](https://prototype.php.net/versions/7.4/install/iis)

 [MySQL 5.7 || MySQL 8](https://dev.mysql.com/downloads/installer/)

## Instalação

#### comands

 ##### instalar as dependencias

```sh
composer install

```

 ##### Criar a key de acesso

 ```sh
php artisan key:generate

```

 #### Rodar as Migrations
 
 ```sh
php artisan migrate

```

## Descrição 

Esta API consiste no gerenciamento de Produtos - Simples, permite que o usuário possa ter controle sobre seus produtos podendo adicionar, remover, editar e ver o seu produto, viabilizando o a automação de trabalhos manuais.


## Routes 

| Detalhe | Rota |
| ------ | ------ |
| Produtos-all    | [/api/produtos] |
| Produtos-create | [/api/cadastrar-produto]|
| Produtos-show   | [/api/detalhes-produto/{id}] |
| Produtos-update | [/api/editar-produto/{id}] |
| Produtos-delete | [/api/remover-produto/{id}] |



<?php

namespace App\Http\Controllers;

use App\Models\DescProduto;
use Illuminate\Http\Request;
use App\Models\Produtos;

class ProdutosController extends Controller
{

    //CRUD

    public function index()
    {
        $produtos = Produtos::all();
        $descProduto = DescProduto::all();
        if (strlen($produtos) != 2) {
            return response()->json([
                'status' => 200,
                'produto' => $produtos,
                'description' => $descProduto,
            ]);
        } else {
            return response()->json([
                'status' => 404,
                'message' => 'produtos não encontrado'
            ]);
        }
    }


    public function store(Request $request)
    {
        $produto = Produtos::create([
            'name' => $request->name,
        ]);
        $descProduto = DescProduto::create([
            'description' => $request->description,
            'value' => $request->value,
            'id_produto' => $produto->id
        ]);

        if ($produto && $descProduto) {
            return response()->json([
                'status' => 200,
                'message' => 'Produto criado com sucesso'
            ]);
        } else {
            return response()->json([
                'status' => '500',
                'message' => 'Erro no servidor'
            ]);
        }
    }
    public function show($id)
    {
        $produto = Produtos::find($id);
        if ($produto) {
            $descProduto = DescProduto::where('id_produto', '=', $produto->id)->get();

            return response()->json([
                'status' => 200,
                'produto' => $produto,
                'description' => $descProduto
            ]);
        } else {
            return response()->json([
                'status' => 404,
                'message' => 'produtos não encontrado'
            ]);
        }
    }



    public function update(Request $request, $id)
    {
        $produto = Produtos::find($id);
        if ($produto) {
            $produto->update([
                'name' => $request->name,
            ]);
            DescProduto::where('id_produto', '=', $produto->id)->update([
                'description' => $request->description,
                'value' => $request->value,
            ]);

            return response()->json([
                'status' => 200,
                'message' => 'Produto editado com sucesso'
            ]);
        } else {
            return response()->json([
                'status' => 404,
                'message' => 'produtos não encontrado'
            ]);
        }
    }

    public function delete($id)
    {
        $produto = Produtos::find($id);
        if ($produto) {
            DescProduto::where('id_produto', '=', $produto->id)->delete();
            $produto->delete();
            return response()->json([
                'status' => 200,
                'message' => 'Produto removido com sucesso'
            ]);
        } else {
            return response()->json([
                'status' => 404,
                'message' => 'produtos não encontrado'
            ]);
        }
    }
}

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class DescProduto extends Model
{
    use HasFactory; 
    protected $fillable = [
        'description',
        'id_produto',
        'value',
    ];

}

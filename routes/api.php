<?php

use App\Http\Controllers\ProdutosController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/


// GET
    // Lista todo os produtos
        Route::get('/produtos' ,[ProdutosController::class, 'index']);
    // Detalha o produto 
        Route::get('/detalhes-produto/{id}', [ProdutosController::class, 'show']);
    // Remove produto
        Route::get('/remover-produto/{id}', [ProdutosController::class, 'delete']);

// POST
    // Cadastra produto
        Route::post('/cadastrar-produto', [ProdutosController::class, 'store']);
    // Edita produto
        Route::post('/editar-produto/{id}',  [ProdutosController::class, 'update']);


<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;
use App\Models\Produtos;
use App\Models\DescProduto;

class CadastrarProdutoTest extends TestCase
{

    use RefreshDatabase, WithFaker;

    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function testCadastroDeProduto()
    {
        $produto = [
            'name' => $this->faker->name,
            'value' => $this->faker->randomFloat(2, 10, 100),
            'description' => $this->faker->sentence
        ];

        $response = $this->post('http://192.168.15.47:8000/api/cadastrar-produto', $produto);

        $this->assertDatabaseHas('produtos', [
            'name' => $produto['name'],
        ]);

        $response->assertStatus(200);
    }
}